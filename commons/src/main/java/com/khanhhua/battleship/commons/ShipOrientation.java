package com.khanhhua.battleship.commons;

public enum ShipOrientation {
    HORIZONTAL,
    VERTICAL
}
